package com.gstoredemomvp.demomvp.thirdparty;

import com.gstoredemomvp.demomvp.model.UserAccessToken;
import retrofit2.Call;
import retrofit2.http.*;

public interface UserServiceApiClient {
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("oauth/token")
    Call<UserAccessToken> authUser(@Header("Authorization") String authorization,
                                   @Query("username") String username,
                                   @Query("password") String password,
                                   @Query("grant_type") String grantType);

//    @Headers("Content-Type: application/x-www-form-urlencoded")
//    @GET("admin/users/{gid}")
//    Call<UserProfile> getUser(@Header("Authorization") String authorization,
//                              @Path("gid") String gid);

}
