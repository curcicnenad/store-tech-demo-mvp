package com.gstoredemomvp.demomvp.thirdparty;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;

import static retrofit2.converter.jackson.JacksonConverterFactory.create;

@Configuration
public class PaymentServiceConfiguration {
    @Value("${core.payment.service}")
    private String serviceUrl;
    private ObjectMapper objectMapper;
    @Autowired
    public PaymentServiceConfiguration(ObjectMapper objectMapper){
        this.objectMapper=objectMapper;
    }

    @Bean
    public Retrofit paymentServiceRetrofitBuilder(){
        return new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(create(objectMapper))
                .build();
    }

    @Bean
    public PaymentServiceApiClient paymentServiceApiClient(){
        return paymentServiceRetrofitBuilder().create(PaymentServiceApiClient.class);
    }
}
