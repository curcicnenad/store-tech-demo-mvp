package com.gstoredemomvp.demomvp.thirdparty;

import com.gstoredemomvp.demomvp.model.*;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.ArrayList;

public interface PaymentServiceApiClient {
    @Headers("Content-Type: application/json")
    @GET("account")
    Call<ArrayList<UserAccountInfo>> accountInfo(@Header("Authorization") String authorization);

    @Headers("Content-Type: application/json")
    @POST("payment/reserve-funds")
    Call<InvoiceData> reserveFunds(@Header("Authorization") String authorization, @Body ReserveFunds funds);

    @Headers("Content-Type: application/json")
    @POST("payment/debit")
    Call<Void> debitFunds(@Header("Authorization") String authorization, @Body DebitFunds funds);
}
