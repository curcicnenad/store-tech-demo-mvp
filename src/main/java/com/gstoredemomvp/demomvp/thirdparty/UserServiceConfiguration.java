package com.gstoredemomvp.demomvp.thirdparty;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;

import static retrofit2.converter.jackson.JacksonConverterFactory.create;

@Configuration
public class UserServiceConfiguration {
    @Value("${oauth2.endpoint}")
    private String userServiceBaseUrl;

    private ObjectMapper objectMapper;

    @Autowired
    public UserServiceConfiguration(ObjectMapper objectMapper)
    {
        this.objectMapper = objectMapper;
    }

    @Bean
    public Retrofit userServiceRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(userServiceBaseUrl)
                .addConverterFactory(create(objectMapper))
                .build();
    }

    @Bean
    public UserServiceApiClient userServiceApiClient() {
        return userServiceRetrofitBuilder().create(UserServiceApiClient.class);
    }
}
