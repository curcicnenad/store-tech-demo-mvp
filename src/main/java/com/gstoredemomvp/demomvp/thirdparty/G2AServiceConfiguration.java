package com.gstoredemomvp.demomvp.thirdparty;

import com.fasterxml.jackson.databind.ObjectMapper;
import retrofit2.Retrofit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static retrofit2.converter.jackson.JacksonConverterFactory.create;

@Configuration
public class G2AServiceConfiguration {

    @Value("${product.service.url}")
    private String productServiceBaseUrl;

    private ObjectMapper objectMapper;

    @Autowired
    public G2AServiceConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Bean
    public Retrofit productServiceRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(productServiceBaseUrl)
                .addConverterFactory(create(objectMapper))
                .build();
    }

    @Bean
    public G2AServiceApiClient g2AServiceClient() {
        return productServiceRetrofitBuilder().create(G2AServiceApiClient.class);
    }

}
