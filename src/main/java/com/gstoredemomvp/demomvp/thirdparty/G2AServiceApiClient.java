package com.gstoredemomvp.demomvp.thirdparty;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.gstoredemomvp.demomvp.model.Order;
import com.gstoredemomvp.demomvp.model.Transaction;
import com.gstoredemomvp.demomvp.model.ProductKey;
import com.gstoredemomvp.demomvp.model.g2a.OrderRequest;
import org.springframework.web.bind.annotation.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.*;

public interface G2AServiceApiClient {

    @Headers({"Content-Type: application/json"})
    @POST("order")
    Call<Order> createOrder(@Header("Authorization") String authorization, @Body OrderRequest oderRequest);

    @PUT("order/pay/{id}")
    @Headers("Content-Type: application/json")
    Call<Transaction> payForAnOrder(@Header("Authorization") String authorization, @Path("id") int id);

    @GET("order/key/{id}")
    @Headers("Content-Type: application/json")
    Call<ProductKey> getProductKey(@Header("Authorization") String authorization, @Path("id") int id);
}
