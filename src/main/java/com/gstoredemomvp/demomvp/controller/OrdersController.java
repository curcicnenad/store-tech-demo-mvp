package com.gstoredemomvp.demomvp.controller;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gstoredemomvp.demomvp.model.g2a.OrderRequest;
import com.gstoredemomvp.demomvp.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.http.Field;

@Controller
@RequestMapping("/orders")
public class OrdersController {
    private OrderService orderService;

    @Autowired
    public OrdersController(OrderService orderService) {
        this.orderService = orderService;
    }

    @CrossOrigin
    @PostMapping(value = "/placeorder")
    public ResponseEntity placeOrder(@RequestBody OrderRequest orderRequest){
        return new ResponseEntity(orderService.placeOrder(orderRequest), HttpStatus.OK);
    }


    @CrossOrigin
    @GetMapping(value = "/history")
    public ResponseEntity getOrdersHistory(){
        return new ResponseEntity(orderService.getOrdersHistory(new Long(1)), HttpStatus.OK);
    }

    @PostMapping(value = "/webhooks/fundsreserved")
    public ResponseEntity fundsReserved(){

        return new ResponseEntity(HttpStatus.OK);
    }
}
