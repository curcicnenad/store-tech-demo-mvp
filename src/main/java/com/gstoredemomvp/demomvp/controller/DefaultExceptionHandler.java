package com.gstoredemomvp.demomvp.controller;

import com.gstoredemomvp.demomvp.Exceptions.G2AException;
import com.gstoredemomvp.demomvp.Exceptions.GNException;
import lombok.Data;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {
    private final String g2aUrl = "//g2aurl";
    private final String gnurl = "//gnurl";

    @ExceptionHandler(G2AException.class)
    public final ResponseEntity<ErrorMessage> g2AException(G2AException ex) {
        ErrorMessage eMessage = new ErrorMessage(ex.getMessage(), ex.getCode(), ex.getUrl());
        HttpStatus status = HttpStatus.valueOf(ex.getCode());
        return new ResponseEntity<ErrorMessage>(eMessage, new HttpHeaders(), status);
    }

    @ExceptionHandler(GNException.class)
    public final ResponseEntity<ErrorMessage> gNException(GNException ex) {
        ErrorMessage eMessage = new ErrorMessage(ex.getMessage(),ex.getCode(), ex.getUrl());
        HttpStatus status = HttpStatus.valueOf(ex.getCode());
        return new ResponseEntity<ErrorMessage>(eMessage, new HttpHeaders(), status);
    }

    @ExceptionHandler(Exception.class)
    public  final ResponseEntity<ErrorMessage> defaultHandler(Exception ex) {
        ErrorMessage eMessage = new ErrorMessage(ex.getMessage());
        return new ResponseEntity<ErrorMessage>(eMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

@Data
class ErrorMessage {
    private String message;
    private String url;
    private int code;

    public ErrorMessage(String message, int code, String url) {
        this.message = message;
        this.code = code;
        this.url = url;
    }

    public  ErrorMessage(String message) {
        this.message = message;
        this.code = 0;
        this.url = null;
    }

}
