package com.gstoredemomvp.demomvp.controller;

import com.gstoredemomvp.demomvp.model.UserAccessToken;
import com.gstoredemomvp.demomvp.service.PaymentService;
import com.gstoredemomvp.demomvp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {
    private UserService userService;
    private PaymentService paymentService;

    @Autowired
    public UserController (UserService userService, PaymentService paymentService) {

        this.userService = userService;
        this.paymentService = paymentService;
    }

//    @PostMapping(value = "/auth")
//    public ResponseEntity<UserAccessToken> getAccessToken(){
//        ResponseEntity <UserAccessToken> re = new ResponseEntity(userService.getAccessToken("shadowue" ,"Mojalozinka82"), HttpStatus.OK);
//        return re;
//    }

    @CrossOrigin
    @GetMapping(value = "/balance")
    public ResponseEntity getBalance() {
        return new ResponseEntity(paymentService.getBalance(), HttpStatus.OK);
    }
}
