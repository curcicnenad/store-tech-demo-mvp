package com.gstoredemomvp.demomvp.Exceptions;

import lombok.Data;

@Data
public class G2AException extends RuntimeException {

    private String url;
    private int code;

    public G2AException () {};
    public G2AException (String message, int code, String url) {
        super(message);
        this.url = url;
        this.code = code;
    }
}
