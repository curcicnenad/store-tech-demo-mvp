package com.gstoredemomvp.demomvp.Exceptions;

import lombok.Data;


@Data
public class GNException extends RuntimeException{

    private String url;
    private int code;

    public GNException () {}

    public GNException (String message, int code, String url) {
        super(message);
        this.url = url;
        this.code = code;
    }
}


