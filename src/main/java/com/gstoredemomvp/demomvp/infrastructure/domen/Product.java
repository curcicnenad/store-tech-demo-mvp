package com.gstoredemomvp.demomvp.infrastructure.domen;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name= "productsHistory")
@Data
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @JsonProperty("user_id")
    @Column(name = "user_id")
    private Long userID;

    @JsonProperty("product_key")
    @Column(name = "product_key")
    private String productKey;

    @JsonProperty("order_id")
    @Column(name = "order_id")
    private int orderID;

    @JsonProperty("transaction_id")
    @Column(name = "transaction_id")
    private String transactionID;

    @JsonProperty("status")
    @Column(name = "status")
    private String status;

    @JsonProperty("product_name")
    @Column(name = "product_name")
    private String productName;

    @JsonProperty("invoice_id")
    @Column(name = "invoice_id")
    private String invoiceId;

    @JsonProperty("price")
    @Column(name = "price")
    private double price;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

}

