package com.gstoredemomvp.demomvp.infrastructure;

import com.gstoredemomvp.demomvp.infrastructure.domen.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
