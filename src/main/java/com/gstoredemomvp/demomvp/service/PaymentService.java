package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.model.DebitFunds;
import com.gstoredemomvp.demomvp.model.InvoiceData;
import com.gstoredemomvp.demomvp.model.ReserveFunds;
import com.gstoredemomvp.demomvp.model.UserAccountInfo;

public interface PaymentService {
    UserAccountInfo getBalance();
    InvoiceData reserveFunds(ReserveFunds funds);
    void debitFunds(DebitFunds funds);
}
