package com.gstoredemomvp.demomvp.service;

import com.sendgrid.*;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmailServiceImpl implements EmailService{
    private static SendGrid sendGrid;
    @Value("${sendgrid.api.key}")
    private String API_KEY;
    private Email fromAddress = new Email("no-reply@SuperbStore.com");

    EmailServiceImpl(){
    }

    @PostConstruct
    private void init(){
        this.sendGrid = new SendGrid(API_KEY);
    }

    @Override
    public boolean sendMail(String toAddress, String subject, String content ) {
        Content contentMail = new Content("text/html",content);
        Email toEmail = new Email(toAddress);
        Mail mail = new Mail(fromAddress, subject, toEmail, contentMail);

        Request request = new Request();
       try{
           request.setMethod(Method.POST);
           request.setEndpoint("mail/send");
           request.setBody(mail.build());
           log.info(">>>>>> sandgrid mail used");
           Response response = sendGrid.api(request);

          // sendGrid.api(request);
          return true;
       }catch(Exception e){
           System.out.println(">>> mail sending error " + e.getMessage());
           return false;
       }
    }




}




