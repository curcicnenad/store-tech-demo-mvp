package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.Exceptions.GNException;
import com.gstoredemomvp.demomvp.model.UserAccessToken;
import com.gstoredemomvp.demomvp.thirdparty.UserServiceApiClient;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import retrofit2.Response;

import java.io.IOException;
import java.nio.charset.Charset;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private UserServiceApiClient userServiceApiClient;

    @Value("${merchant.clientid}")
    private String CLIENT_ID;

    @Value("${merchant.secret}")
    private String CLIENT_SECRET;

    @Autowired
    public UserServiceImpl(UserServiceApiClient userServiceApiClient) {
        this.userServiceApiClient = userServiceApiClient;
    }

    @Override
    public UserAccessToken getAccessToken(String userName, String userPass) {
        String credentials = CLIENT_ID + ":" + CLIENT_SECRET;
        String basic = new String(Base64.encodeBase64
                (credentials.getBytes(Charset.forName("US-ASCII"))));
        try {
            Response<UserAccessToken> response = userServiceApiClient
                    .authUser("Basic " + basic, userName, userPass, "password")
                    .execute();
            if(response.isSuccessful()) {
                log.info("token retrived");
                return response.body();
            }else{
                log.info("Error getting user token.");
                log.info(">>>>>>>>> token error code: " + response.code());
                throw new GNException(response.message(), response.code(), response.raw().request().url().toString());
            }
        } catch (final IOException e) {
            System.out.println("getting token error " + e.getMessage());
            return null;
        }
    }

}
