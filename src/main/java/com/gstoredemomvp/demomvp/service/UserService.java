package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.model.UserAccessToken;


public interface UserService {
    UserAccessToken getAccessToken(String userName, String userPass);
}
