package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.infrastructure.domen.Product;
import com.gstoredemomvp.demomvp.model.ProductKey;
import com.gstoredemomvp.demomvp.model.g2a.OrderRequest;
import org.springframework.http.ResponseEntity;
import okhttp3.ResponseBody;
import retrofit2.Response;

import java.util.List;

public interface OrderService {
    List<ProductKey> placeOrder(OrderRequest orderRequest);
    //ProductKey orderPayment(int orderID);
    List<Product> getOrdersHistory(Long userID);
}
