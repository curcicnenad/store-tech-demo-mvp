package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.Exceptions.G2AException;
import com.gstoredemomvp.demomvp.infrastructure.ProductRepository;
import com.gstoredemomvp.demomvp.model.*;
import com.gstoredemomvp.demomvp.infrastructure.domen.Product;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.time.Instant;

import com.gstoredemomvp.demomvp.model.g2a.OrderRequest;
import com.gstoredemomvp.demomvp.thirdparty.G2AServiceApiClient;
import org.dozer.DozerBeanMapper;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private G2AServiceApiClient g2aClient;
    private EntityManager entityManager;
    private ProductRepository productRepository;

    @Value("${g2a.auth.key}")
    private String AUTH_KEY;

    @Value("${testuser.notificationmail}")
    private String notificationMail;

    @Value("${webhook.reservefunds}")
    private String reserveFundsNotificationUrl;

    private EmailServiceImpl mailService;
    private PaymentService paymentService;
    private DozerBeanMapper mapper;


    @Autowired
    public OrderServiceImpl( G2AServiceApiClient g2aClient,EntityManager entityManager, ProductRepository productRepository, PaymentService paymentService){
        this.entityManager = entityManager;
        this.g2aClient = g2aClient;
        this.entityManager = entityManager;
        this.productRepository = productRepository;
        mailService = new EmailServiceImpl();
        this.paymentService = paymentService;
        this.mapper = new DozerBeanMapper();
    }

    @Override
    public List<ProductKey> placeOrder(OrderRequest orderRequest) {
        Product createdProduct = createOrderAndPersist(orderRequest);
        Product updatedProduct = payOrderAndPersist(createdProduct);
        Product finishedProduct = getProductKeyAndPersist(updatedProduct,orderRequest);

        ProductKey key = new ProductKey();
        key.setTrID(finishedProduct.getTransactionID());
        key.setOrderID(finishedProduct.getOrderID());
        key.setPrice(finishedProduct.getPrice());
        log.info(">>>>>>>>>>> key dohvacen" + finishedProduct.getProductKey());
        key.setPrKey(finishedProduct.getProductKey());

        String mailContent = createMailContent(orderRequest.getProductName(), orderRequest.getProductId(), key.getPrKey());
        //mailService.sendMail("pozegaoffice@gmail.com", "new product key", mailContent);

        UserAccountInfo balance = paymentService.getBalance();
        key.setBalance(balance.getBalance());
        key.setAvailableBalance(balance.getAvailableBalance());
        key.setCount(1);;
        key.setStatus("FINISHED");
        ArrayList<ProductKey> result = new ArrayList();
        result.add(key);
        return result;
    }

    private Product createOrderAndPersist(OrderRequest orderRequest){
        try{
            Product prod = new Product();
            prod.setUserID(new Long(1));
            Response result= g2aClient.createOrder(AUTH_KEY, orderRequest).execute();
            if(result.isSuccessful()){
                Order order = mapper.map(result.body(), Order.class);
                ReserveFunds fund = createReserveRequest(orderRequest,order);
                InvoiceData invoice = paymentService.reserveFunds(fund);

                //persist to db
                prod.setOrderID(order.getOrderID());
                prod.setStatus("Created");
                prod.setProductName(orderRequest.getProductName());
                prod.setInvoiceId(invoice.getInvoiceId());
                prod.setPrice(orderRequest.getPrice()*1000);

                return productRepository.save(prod);
            }else{
                log.info("throwan g2a exception");
                throw new G2AException(result.message(), result.code(),result.raw().request().url().toString());
            }
        }catch(IOException exception){
            return null;
        }
    }

    private Product payOrderAndPersist(Product product){
        try{
            Response result = g2aClient.payForAnOrder(AUTH_KEY, product.getOrderID()).execute();
            if(result.isSuccessful()){
                Transaction transaction = mapper.map(result.body(), Transaction.class);
                paymentService.debitFunds(createDebitRequest(product));
                product.setTransactionID(transaction.getTransactionId());
                product.setStatus("Payed");
                return productRepository.save(product);
            }else{
                log.info("throwan g2a exception");
                throw new G2AException(result.message(), result.code(), result.raw().request().url().toString());
            }
        }catch(IOException exception){
            return null;
        }
    }

    private Product getProductKeyAndPersist(Product product, OrderRequest orderRequest){
        try{
            Response response = g2aClient.getProductKey(AUTH_KEY, product.getOrderID()).execute();
            if(response.isSuccessful()){

                ProductKey key = mapper.map(response.body(), ProductKey.class);
                log.info(">>> dohvacen product key: " + key.getPrKey());
                product.setProductKey(key.getPrKey());
                product.setStatus("Finished");
                return productRepository.save(product);
            }else{
                log.info("throwan g2a exception");
                throw new G2AException(response.message(), response.code(), response.raw().request().url().toString());
            }
        }catch(IOException exception){
            return null;
        }
    }



    private String createMailContent(String productName, String productID, String productKey) {
        String result = "You bought a new product\n" +
                "<br>product ID: " + productID + "\n" +
                "<br>product name: " + productName + "\n" +
                "<br>product key: " + productKey;
        return result;
    }

    private ReserveFunds createReserveRequest (OrderRequest orderRequest, Order order) {
        ReserveFunds result = new ReserveFunds();
        result.setAmount(orderRequest.getPrice() * 1000);
        result.setCreatedTimestamp(Long.toString(Instant.now().getEpochSecond()));
        result.setCurrency("gnationgold");
        result.setOrderDescription(orderRequest.getProductName());
        result.setOrderId(Integer.toString(order.getOrderID()));
        result.setNotifyUrl(reserveFundsNotificationUrl);
        return  result;
    }

    private DebitFunds createDebitRequest (Product data) {
        DebitFunds result = new DebitFunds();
        result.setInvoiceId(data.getInvoiceId());
        return result;
    }

    @Override
    public List<Product> getOrdersHistory(Long userID) {
        log.info(">>>>>> pogodjen history: " + userID);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);

        Root<Product> order = cq.from(Product.class);
        Predicate userIDPredicate = cb.equal(order.get("userID"), userID);
        cq.where(userIDPredicate);

        TypedQuery<Product> query = entityManager.createQuery(cq);
        List<Product> products = query.getResultList();
        Collections.reverse(products);
        return products;
    }

}
