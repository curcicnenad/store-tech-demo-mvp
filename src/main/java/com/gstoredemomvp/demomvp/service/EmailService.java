package com.gstoredemomvp.demomvp.service;

public interface EmailService {
    boolean sendMail(String toAddress, String subject, String content);
}
