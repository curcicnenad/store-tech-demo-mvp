package com.gstoredemomvp.demomvp.service;

import com.gstoredemomvp.demomvp.Exceptions.GNException;
import com.gstoredemomvp.demomvp.model.*;
import com.gstoredemomvp.demomvp.thirdparty.PaymentServiceApiClient;
import lombok.extern.slf4j.Slf4j;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private UserService userService;
    private PaymentServiceApiClient paymentServiceApiClient;
    private DozerBeanMapper mapper;

    @Value("${testuser.username}")
    private String userName;

    @Value("${testuser.pass}")
    private String password;

    @Value("${merchant.agreementid}")
    private String agreementID;

    @Autowired
    public PaymentServiceImpl (UserService userService, PaymentServiceApiClient paymentServiceApiClient) {
        this.userService = userService;
        this.paymentServiceApiClient = paymentServiceApiClient;
        this.mapper = new DozerBeanMapper();
    }


    @Override
    public UserAccountInfo getBalance() {
        try {
            UserAccessToken token = userService.getAccessToken(userName, password);
            ArrayList<UserAccountInfo> userInfo = paymentServiceApiClient.accountInfo("Bearer" + token.getAccessToken()).execute().body();
            Response result = paymentServiceApiClient.accountInfo("Bearer" + token.getAccessToken()).execute();
            if(result.isSuccessful()) {
                ArrayList<UserAccountInfo> info = mapper.map(result.body(), ArrayList.class);
                return info.get(0);
            } else {
                throw new GNException(result.message(),result.code(), result.raw().request().url().toString());
            }
       } catch (IOException e) {
           System.out.println(" get balance error " + e.getMessage());
           return null;
       }
    }

    @Override
    public InvoiceData reserveFunds(ReserveFunds funds) {
        try {
            UserAccessToken token = userService.getAccessToken(userName, password);
            funds.setAgreementId(agreementID);
             Response result = paymentServiceApiClient.reserveFunds("Bearer " + token.getAccessToken(),funds).execute();
            if(result.isSuccessful()){
                InvoiceData data = mapper.map(result.body(), InvoiceData.class);
                return data;
            }else{
                throw new GNException(result.message(),result.code(), result.raw().request().url().toString());
            }
        }catch (IOException e) {
            log.info("reserve funds error" + e.getMessage());
            return  null;
        }

    }

    @Override
    public void debitFunds(DebitFunds funds) {
        try {
            UserAccessToken token = userService.getAccessToken(userName, password);
            funds.setAgreementId(agreementID);
            Response result = paymentServiceApiClient.debitFunds("Bearer " + token.getAccessToken(),funds).execute();
            if(!result.isSuccessful()){
             throw new GNException(result.message(),result.code(), result.raw().request().url().toString());
            }
        }catch (IOException e) {
            System.out.println("debit funds error " + e.getMessage());

        }
    }
}
