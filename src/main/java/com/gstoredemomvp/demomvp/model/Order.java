package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class Order {
    @JsonProperty("order_id")
    private int orderID;

    @JsonProperty("price")
    private double price;

    @JsonProperty("currency")
    private String currency;
}
