package com.gstoredemomvp.demomvp.model.g2a;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OrderRequest {

    @JsonProperty("product_id")
    String productId;

    @JsonProperty("price")
    double price;

    @JsonProperty("product_name")
    String productName;
}
