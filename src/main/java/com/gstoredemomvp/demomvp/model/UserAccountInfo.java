package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "currency",
        "balance",
        "available_balance",
        "account_id",
        "status"
})
@Data
public class UserAccountInfo {

    @JsonProperty("currency")
    String currency;
    @JsonProperty("balance")
    double balance;
    @JsonProperty("available_balance")
    double availableBalance;
    @JsonProperty("account_id")
    String accountID;

    @JsonProperty("status")
     String status;



//    @JsonProperty("gid")
//    private String gid;
//    @JsonProperty("currency")
//    private String currency;
//    @JsonProperty("account_id")
//    private String accountId;
//    @JsonProperty("account_type")
//    private String accountType;
//    @JsonProperty("status")
//    private String status;
//    @JsonProperty("balance")
//    private double balance;
//    @JsonProperty("available_balance")
//    private double availableBalance;
}


