package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "key",
        "transaction_id",
        "order_id",
        "price",
        "balance",
        "available_balance",
        "count",
        "order_status"
})

@Data
public class ProductKey {
    @JsonProperty("key")
    private String prKey;

    @JsonProperty("transaction_id")
    private String trID;

    @JsonProperty("order_id")
    private int orderID;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("balance")
    private Double balance;

    @JsonProperty("available_balance")
    private Double availableBalance;

    @JsonProperty("count")
    private int count;

    @JsonProperty("order_status")
    private String status;
}
