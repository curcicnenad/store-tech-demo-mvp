package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class InvoiceData {
    @JsonProperty("created_timestamp")
    private Date createdTimestamp;
    @JsonProperty("expired_timestamp")
    private Date expiredTimestamp;
    @JsonProperty("invoice_id")
    private String invoiceId;
}
