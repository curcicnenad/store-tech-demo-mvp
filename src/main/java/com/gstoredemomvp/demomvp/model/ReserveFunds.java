package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ReserveFunds {
    @JsonProperty("agreement_id")
    private String agreementId;
    @JsonProperty("amount")
    private double amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("created_timestamp")
    private String createdTimestamp;
    @JsonProperty("notify_url")
    private String notifyUrl;
    @JsonProperty("order_description")
    private String orderDescription;
    @JsonProperty("order_id")
    private String orderId;
}
