package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DebitFunds {
    @JsonProperty("agreement_id")
    private String agreementId;
    @JsonProperty("invoice_id")
    private String invoiceId;
}
