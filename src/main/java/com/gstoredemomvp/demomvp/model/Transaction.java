package com.gstoredemomvp.demomvp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "transaction_id"
})
@Data
public class Transaction {
    @JsonProperty("status")
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public boolean getStatus() {
        return status;
    }

    public  void setStatus(boolean status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty("transaction_id")

    private String transactionId;
}
